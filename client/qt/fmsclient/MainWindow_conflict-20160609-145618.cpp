#include <QGuiApplication>
#include <QGraphicsScene>
#include <QFileDialog>
#include <QTime>
#include <QFile>
#include <QElapsedTimer>

#include <QNodeViewBlock.h>
#include <QNodeViewEditor.h>
#include <QNodeViewPort.h>
#include <QNodeViewCanvas.h>

#include <QXMLWorker.h>

#include "MainWindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QDir>

QString fmsconfig = QDir::homePath() + "/.mfmtools/fmsconfig.xml";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    defineButtons();
    defineNodeview();
    defineCounter();
    defineTreeview();
}

void MainWindow::test1()
{
}

void MainWindow::test2()
{
}

void MainWindow::test3()
{
}

void MainWindow::defineButtons()
{
    QObject::connect(ui->pushButtonStart, SIGNAL(clicked()), this, SLOT(startSim()));
    QObject::connect(ui->pushButtonStop, SIGNAL(clicked()), this, SLOT(stopSim()));
    QObject::connect(ui->pushButtonPause, SIGNAL(clicked()), this, SLOT(pauseSim()));
    QObject::connect(ui->pushButtonTest1, SIGNAL(clicked()), this, SLOT(test1()));
}

void MainWindow::defineCounter()
{
    minutes=0;
    seconds=0;
    timer = new QTimer();
    timeValue = new QTime(0,minutes,seconds,0);
    ui->lcdNumber->display(timeValue->toString(QString("h:mm:ss:zzz")));
    connect(timer, SIGNAL(timeout()), this, SLOT(setDisplay()));
}

void MainWindow::defineNodeview()
{
    m_scene = new QGraphicsScene();
    m_view = new QNodeViewCanvas(m_scene, this);
    setCentralWidget(m_view);

    m_editor = new QNodeViewEditor(this);
    m_editor->install(m_scene);

    addBlockInternal(QPointF(0, 0));
    addBlockInternal(QPointF(150, 0));
    addBlockInternal(QPointF(150, 150));
}

void MainWindow::defineTreeview()
{
    // Below is not working for some really weird reason. try this:
    // http://www.bogotobogo.com/Qt/Qt5_QtXML_QDOM_Reading_XML.php

    //QXMLWorker* xml = new QXMLWorker();
    //xml->readFile(fmsconfig);
    Filename = QDir(qApp->applicationDirPath()).absoluteFilePath("MyXML.xml");
    model = new QStandardItemModel(0,1,this);
    readConfig();
    ui->treeViewModels->setModel(model);
    ui->treeViewModels->expandAll();
}

void MainWindow::readConfig()
{
    QStandardItem *root = new QStandardItem("Books");
    model->appendRow(root);

    QDomDocument document;

    //load the xml file
    QFile file(Filename);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        document.setContent(&file);
        file.close();
    }

    //get the xml root element
    QDomElement xmlroot = document.firstChildElement();

    //read the books
    QDomNodeList books = xmlroot.elementsByTagName("Book");
    for(int i = 0; i < books.count(); i++)
    {
        QDomElement book = books.at(i).toElement();
        QStandardItem *bookitem = new QStandardItem(book.attribute("Name"));

        //read the chapters of the book
        QDomNodeList chapters = book.elementsByTagName("Chapter");
        for(int h = 0; h < chapters.count(); h++)
        {
            QDomElement chapter = chapters.at(h).toElement();
            QStandardItem *chapteritem = new QStandardItem(chapter.attribute("Name"));

            bookitem->appendRow(chapteritem);
        }

        root->appendRow(bookitem);
    }
}

void MainWindow::addBlock()
{
    addBlockInternal(m_view->sceneRect().center().toPoint());
}

void MainWindow::saveFile()
{
    QString s_fileName = QFileDialog::getSaveFileName();
    if (s_fileName.isEmpty())
        return;

    QFile f_file(s_fileName);
    f_file.open(QFile::WriteOnly);
    QDataStream stream(&f_file);
    m_editor->save(stream);
}

void MainWindow::loadFile()
{
    QString s_fileName = QFileDialog::getOpenFileName();
    if (s_fileName.isEmpty())
        return;

    QFile f_file(s_fileName);
    f_file.open(QFile::ReadOnly);
    QDataStream stream(&f_file);
    m_editor->load(stream);
}

void MainWindow::startSim()
{
    //QString msg = "starting the simulation!";
    //logMessage(msg);
    timer->setInterval(1);
    timer->start();
}

void MainWindow::stopSim()
{
    //logMessage("stopping the simulation!");
    timer->stop();
    timeValue = new QTime(0,0,0,0);
    ui->lcdNumber->display(timeValue->toString(QString("h:mm:ss:zzz")));

}

void MainWindow::pauseSim()
{
    //logMessage("pausing the simulation!");
    timer->stop();
}

void MainWindow::addBlockInternal(const QPointF& position)
{
    QNodeViewBlock* block = new QNodeViewBlock(NULL);
    m_scene->addItem(block);

    static qint32 index = 1;
    QString blockName = QString("testModel_%1").arg(index++);

    block->addPort(blockName, 0, QNodeViewPortLabel_Name);
    block->addPort("TestEntity", 0, QNodeViewPortLabel_Type);

    block->addInputPort("Input 1");
    block->addInputPort("Input 2");
    block->addInputPort("Input 3");

    block->addOutputPort("Output 1");
    block->addOutputPort("Output 2");
    block->addOutputPort("Output 3");
    block->addOutputPort("Output 4");
    block->setPos(position);
}

void MainWindow::setDisplay()
{
    this->timeValue->setHMS(0,this->timeValue->addMSecs(-1).minute(),
                        this->timeValue->addMSecs(-1).second(),
                        this->timeValue->addMSecs(-1).msec());
    ui->lcdNumber->display(timeValue->toString(QString("h:mm:ss:zzz")));
}

void MainWindow::showTime()
{
    QTime time = QTime::currentTime();
    QString text = time.toString("hh:mm");
    if ((time.second() % 2 == 0))
        text[2] = ' ';
    ui->lcdNumber->display(text);
}

void MainWindow::logMessage(const QString& msg)
{
    qDebug() << msg;
}

MainWindow::~MainWindow()
{
    delete ui;
}
