#include "MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    w.setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
    w.showMaximized();
    w.show();

    return a.exec();
}
