#!/bin/env python3

import os
import os.path
import sys
import select
import webbrowser
import subprocess
# import requests
import urllib.request
from datetime import datetime
from PyQt5 import Qt, QtWidgets, QtGui, QtCore, QtNetwork
from PyQt5.QtCore import QObject, QThread
from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QSystemTrayIcon, QMenu, QAction
import xml.dom.minidom
from xml.etree import ElementTree as ET
from xml.dom.minidom import Node
import fms_launcher
import paramiko
import zmq


# Global variables
web_server = "ms7.us"
fm_server = "localhost"
fm_port = 5555
sh_port = 2222
g_verbose = True
zmq_server = ""
socket = ""
home = os.path.expanduser('~')


class MainWindow(QtWidgets.QMainWindow, fms_launcher.Ui_Dialog):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.okay_to_close = False

        # Progress Bar
        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)
        self.progressBar.show()

        # Buttons
        self.pushButtonClose.clicked.connect(self.close)
        self.pushButtonFMS.clicked.connect(self.start_fms)
        self.pushButtonFMS.setDisabled(True)

        # Get Settings
        self.tabSettings.setDisabled(True)
        self.textBrowserOutput.append("Retrieving settings...")

        # Table Widget
        self.tableWidget.setRowCount(10)
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setHorizontalHeaderLabels(('Status', 'Package', 'Version', 'Description'))
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.setShowGrid(True)
        self.tableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        # News
        self.textBrowserOutput.append("Retrieving news...")
        self.textBrowserNews.setOpenExternalLinks(True)
        # self.textBrowserNews.setSearchPaths(web_server)
        self.textBrowserNews.setSource(Qt.QUrl("news.html"))
        self.textBrowserNews.setWindowTitle("wtf?")
        self.textBrowserNews.show()

        # TODO: Get version from xml file
        self.labelVersion.setText("A.11")

        # Start Worker Thread
        self.thread = QThread()
        self.downloader = WorkerInit("connect", "init")
        self.downloader.finished[str].connect(self.on_finished)
        self.downloader.moveToThread(self.thread)
        self.thread.started.connect(self.downloader.work)
        self.thread.start()

    @pyqtSlot(str)
    def on_finished(self, key):

        print("%s complete" % key)

        # TODO: Get tools from xml file on server and compare with local installed
        # print(dom)
        # root = ET.fromstring(dom)
        # tree = xml.dom.minidom.parse(dom)
        # root = dom.documentElement
        # webserver = root.getElementsByTagName("webserver")
        # self.textBrowserOutput.append("webserver: %s" % webserver)
        # self.tableWidget.setItem(0, 0, QtWidgets.QTableWidgetItem("PDCU"))
        # self.tableWidget.openPersistentEditor(0)
        # self.tableWidget.setItemDelegateForColumn(0, PushButtonDelegate(self))

        # Enable settings tab
        self.progressBar.setMaximum(100)
        self.progressBar.setValue(100)
        self.tabSettings.setEnabled(True)
        self.textBrowserOutput.append("Settings retrieved.")

    def set_style(self):
        css = "style.css"
        with open(css, "r") as fh:
            self.setStyleSheet(fh.read())

    def close(self):
        self.hide()

    def closeEvent(self, event):
        if self.okay_to_close:
            super(MainWindow, self).closeEvent(event)
        else:
            event.ignore()
            self.hide()

    def start_fms(self):
        self.textBrowserOutput.append("Not implemented.")


class RightClickMenu(QMenu):
    def __init__(self, parent=None):
        QMenu.__init__(self, "File", parent)

        icon_web = QtGui.QIcon(":/images/NASALogo.png")
        action_web = QAction(icon_web, "M&FM Lab &Website", self)
        action_web.triggered.connect(lambda: webbrowser.open('https://mfmlab.msfc.nasa.gov'))
        self.addAction(action_web)

        icon_fms = QtGui.QIcon(":/images/fms-icon-desktop.png")
        action_fms = QAction(icon_fms, "M&FM &Tools Manager", self)
        action_fms.triggered.connect(self.show_launcher)
        self.addAction(action_fms)

        self.addSeparator()

        icon_exit = QtGui.QIcon(":/images/application-exit.png")
        action_exit = QAction(icon_exit, "&Exit", self)
        action_exit.triggered.connect(lambda: close_app(0))
        self.addAction(action_exit)

    @staticmethod
    def show_launcher():
        fmsapp.show()


class SystemTrayIcon(QSystemTrayIcon):
    def __init__(self, parent=None):
        QSystemTrayIcon.__init__(self, parent)
        self.setIcon(QtGui.QIcon(":/images/fms-icon-desktop.png"))
        self.right_menu = RightClickMenu()
        self.setContextMenu(self.right_menu)
        self.activated.connect(self.on_tray_icon_activated)

    def show(self):
        QSystemTrayIcon.show(self)

    @staticmethod
    def on_tray_icon_activated(reason):
        if reason == QSystemTrayIcon.DoubleClick:
            fmsapp.show()


class PushButtonDelegate(Qt.QItemDelegate):
    def __init__(self, parent):
        Qt.QItemDelegate.__init__(self, parent)

    def createEditor(self, parent, option, index):
        button = QtWidgets.QPushButton(str(index.data()), parent)
        button.clicked.connect(self.current_index_changed)
        return button

    def setEditorData(self, editor, index):
        editor.blockSignals(True)
        editor.blockSignals(False)

    def setModelData(self, editor, model, index):
        model.setData(index, editor.text())

    @pyqtSlot()
    def current_index_changed(self):
        self.commitData.emit(self.sender())


class Download(QtNetwork.QNetworkAccessManager):

    def slot_finished(self, dfile):
        file = QtCore.QFile(home + '/Downloads/' + dfile)

        if file.open(QtCore.QIODevice.WriteOnly):
            file.write(self.messageBuffer)
            file.close()
            QtWidgets.QMessageBox.information(None, "Hello!", "File has been saved!")
        else:
            QtWidgets.QMessageBox.critical(None, "Hello!", "Error saving file!")

    # Append current data to the buffer every time readyRead() signal is emitted
    def slot_read_data(self):
        self.messageBuffer += self.reply.readAll()

    def __init__(self, link, file):
        QtNetwork.QNetworkAccessManager.__init__(self)
        self.messageBuffer = QtCore.QByteArray()
        url = QtCore.QUrl(link + file)
        req = QtNetwork.QNetworkRequest(url)
        self.reply = self.get(req)
        self.reply.readyRead.connect(self.slot_read_data)
        self.reply.finished.connect(lambda: self.slot_finished(file))


class WorkerInit (QObject):

    finished = pyqtSignal(str)

    def __init__(self, key, val):
        super().__init__()
        self.key = key
        self.val = val

    def work(self):

        if self.key == "connect":
            print("Connecting!")
        elif self.key == "ssh":
            print("creating ssh keys!")
        elif self.key == "install":
            print("Installing application!")
        elif self.key == "update":
            print("Updating Application!")
        elif self.key == "uninstall":
            print("Uninstalling application!")
        elif self.key == "query":
            print("Querying application versions!")
        else:
            print("Unknown command")

        print("%s:%s" % (self.key, self.val))

        print("worker thread complete")
        self.finished.emit(self.key)
        # dl = Download('http://mfmlab.msfc.nasa.gov/', 'easetupfull.exe')

        # xmlurl = "http://" + web_server
        # xmlpath = xmlurl + "/fms.xml"
        # try:
        #     xmldoc = urllib.request.urlopen(xmlpath)
        #     dom = xmldoc.read()
        #    root = ET.fromstring(dom)
        #     self.finished.emit('%s' % dom)
        # except Exception as e:
        # print(e)
        # root = dom.documentElement
        # webserver = root.getElementsByTagName("webserver")
        # print(webserver)
        # webserver = "m7s.us"
        # self.textBrowserOutput.append("webserver: %s" % webserver)


def ssh(web_server):
    print(web_server)


def verbose(s):
    if g_verbose:
        s = "VERBOSE: " + s
        fmsapp.textEditOutput.append(s)


def log(txt):
    fmsapp.textEditOutput.append(txt)
    fmsapp.textEditOutput.ensureCursorVisible()


def close_app(rc):
    exit(rc)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    if os.path.isfile(home + "/.mfmtools/fmsconfig.xml"):
        print("user has fmsconfig.xml")
    else:
        print("Nope!  first time user!!!")

    tray = SystemTrayIcon()
    tray.show()

    fmsapp = MainWindow()
    fmsapp.set_style()
    fmsapp.show()
    sys.exit(app.exec_())
