#!/usr/bin/env python

from PyQt5.QtCore import QModelIndex, pyqtSlot
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import QApplication, QItemDelegate, QPushButton, QTableView, QAbstractItemView


class PushButtonDelegate(QItemDelegate):
    def createEditor(self, parent, option, pbindex):
        button = QPushButton(str(index.data()), parent)
        button.clicked.connect(lambda: self.current_index_changed(pbindex))
        return button

    @pyqtSlot()
    def current_index_changed(self, pbindex):
        self.commitData.emit(self.sender())
        rowid = pbindex.row()
        toolid = rowid + 1
        print("Tool ID: %s" % toolid)
        tool = model.item(rowid, 1).text()
        print("Tool Name: %s" % tool)


if __name__ == '__main__':

    import sys

    app = QApplication(sys.argv)

    model = QStandardItemModel(4, 2)
    tableView = QTableView()
    tableView.setModel(model)

    delegate = PushButtonDelegate()
    tableView.setItemDelegate(delegate)
    tableView.setEditTriggers(
            QAbstractItemView.DoubleClicked | QAbstractItemView.SelectedClicked)

    for row in range(4):
        for column in range(2):
            index = model.index(row, column, QModelIndex())
            model.setData(index, (row + 1) * (column + 1))
            tableView.openPersistentEditor(model.index(row, 0))

    tableView.setWindowTitle("Push Button Delegate")
    tableView.show()
    sys.exit(app.exec_())
