#-------------------------------------------------
#
# Project created by QtCreator 2016-06-12T15:39:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ssh
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \

FORMS    += mainwindow.ui


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/libssh/lib/ -lssh
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/libssh/lib/ -lsshd
else:unix: LIBS += -L$$PWD/libssh/lib/ -lssh

INCLUDEPATH += $$PWD/libssh/include
DEPENDPATH += $$PWD/libssh/include
