#!/usr/bin/env python

import sys
import getpass
import select
import paramiko
import zmq
import time
import socketserver as SocketServer
import threading
from threading import Event
from time import sleep

# Global Variables
fm_server = "localhost"
fm_port = 5555
sh_port = 22
g_verbose = True
# theserver = ''


class WorkerForward (threading.Thread):
    def __init__(self, thread_id, name, ready=None):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.ready = ready

    def run(self):
        verbose("Starting " + self.name)
        server_tunnel(self)
        verbose("Exiting " + self.name)


class ForwardServer (SocketServer.ThreadingTCPServer):
    daemon_threads = True
    allow_reuse_address = True


class Handler (SocketServer.BaseRequestHandler):

    def handle(self):
        try:
            chan = self.ssh_transport.open_channel('direct-tcpip',
                                                   (self.chain_host, self.chain_port),
                                                   self.request.getpeername())
        except Exception as e:
            verbose('Incoming request to %s:%d failed: %s' % (self.chain_host,
                                                              self.chain_port,
                                                              repr(e)))
            return
        if chan is None:
            verbose('Incoming request to %s:%d was rejected by the SSH server.' %
                    (self.chain_host, self.chain_port))
            return

        verbose('Connected!  Tunnel open %r -> %r -> %r' % (self.request.getpeername(),
                                                            chan.getpeername(), (self.chain_host, self.chain_port)))
        while True:
            r, w, x = select.select([self.request, chan], [], [])
            if self.request in r:
                data = self.request.recv(1024)
                if len(data) == 0:
                    break
                chan.send(data)
            if chan in r:
                data = chan.recv(1024)
                if len(data) == 0:
                    break
                self.request.send(data)

        peername = self.request.getpeername()
        chan.close()
        self.request.close()
        verbose('Tunnel closed from %r' % (peername,))


def server_tunnel(self):

    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.WarningPolicy())

    verbose('Connecting to ssh host %s:%d ...' % (fm_server, sh_port))
    try:
        client.connect(fm_server, sh_port, username=getpass.getuser(), look_for_keys=True)
    except Exception as e:
        print('*** Failed to connect to %s:%d: %r' % (fm_server, sh_port, e))
        sys.exit(1)

    verbose('Starting Fault Managmement Simulator Server...')
    try:
        client.exec_command("~/mach7/zmq/fmsd")
    except Exception as e:
        print('*** Unable to start FMS Server: %s ...' % e)
        sys.exit(2)

    self.ready.set()

    verbose('Now forwarding port %d to %s:%d ...' % (fm_port, fm_server, fm_port))

    try:
        forward_tunnel(fm_port, fm_server, fm_port, client.get_transport())
    except KeyboardInterrupt:
        print('C-c: Port forwarding stopped.')
        sys.exit(0)


def forward_tunnel(local_port, remote_host, remote_port, transport):

    global zmq_server

    class SubHander (Handler):
        chain_host = remote_host
        chain_port = remote_port
        ssh_transport = transport

    zmq_server = ForwardServer(('', local_port), SubHander)
    zmq_server.serve_forever()


def verbose(s):
    if g_verbose:
        print(s)


def fms_exit(rc):
    zmq_server.shutdown()
    sys.exit(rc)


def main():
    ready = Event()
    thread_tunnel = WorkerForward(1, "WorkerForward", ready)
    thread_tunnel.start()

    verbose("Exiting Main Thread")

    ready.wait()
    sleep(5)
    print("Connecting to hello world server...")
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5555")
    socket.send(b"01001")
    message = socket.recv()

    if message == b"01001":
        print("we did it!!!")

    print("Received reply [ %s ]" % message)
    socket.send(b"01000")
    message = socket.recv()
    print("Received reply [ %s ]" % message)
    fms_exit(0)

if __name__ == '__main__':
    main()

