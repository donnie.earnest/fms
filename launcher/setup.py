#! python3.4
from setuptools import setup
import os
import py2exe
import matplotlib

includes = ["sip",
            "PyQt5",
            "PyQt5.QtCore",
            "PyQt5.QtGui",
            "PyQt5.QtWidgets",
            "urllib.request",
            "webbrowser",
            "numpy",
            "matplotlib.backends.backend_qt5agg",
            "pandas"]

datafiles = [("platforms", ["E:\\Python34\\Lib\\site-packages\\PyQt5\\plugins" +
                            "\\platforms\\qwindows.dll"]),
             ("", [r"c:\windows\syswow64\MSVCP100.dll",
                   r"c:\windows\syswow64\MSVCR100.dll"])] + \
            matplotlib.get_py2exe_datafiles()

setup(
    name='mfmlab',
    version=3.0,
    # packages=['launcher'],
    url='',
    license='',
    windows=[{"script": "fms.py"}],
    scripts=['fms.py'],
    data_files=datafiles,
    install_requires=['numpy>=1.8.1',
                      'matplotlib>=1.4.2',
                      'pandas>=0.14.1'],
    options={
        "py2exe":{
            "includes": includes,
        }
    }
)