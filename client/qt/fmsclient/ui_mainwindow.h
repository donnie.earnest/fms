/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionE_xit;
    QAction *action_New;
    QAction *action_Open;
    QAction *action_Save;
    QAction *action_Add_Node;
    QAction *actionSave_As;
    QAction *actionOpen_Recent;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget_2;
    QWidget *tab_5;
    QWidget *tab_6;
    QMenuBar *menuBar;
    QMenu *menuE_xit;
    QMenu *menu_Edit;
    QStatusBar *statusBar;
    QDockWidget *dockWidgetNodes;
    QWidget *dockWidgetLeftContents;
    QGridLayout *gridLayout_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_8;
    QTabWidget *tabWidgetNodes;
    QWidget *tab;
    QGridLayout *gridLayout_9;
    QTreeView *treeViewModels;
    QWidget *tab_2;
    QGridLayout *gridLayout_10;
    QTreeView *treeViewManagers;
    QWidget *tab_3;
    QGridLayout *gridLayout_11;
    QTreeView *treeViewWidgets;
    QDockWidget *dockWidgetToolbar;
    QWidget *dockWidgetTopContents;
    QGridLayout *gridLayout_2;
    QFrame *frame_3;
    QGridLayout *gridLayout_6;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QPushButton *pushButtonSchedule;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_2;
    QPushButton *pushButtonStop;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButtonSave;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButtonStep;
    QPushButton *pushButtonStart;
    QPushButton *pushButtonNew;
    QPushButton *pushButtonPause;
    QLCDNumber *lcdNumber;
    QPushButton *pushButtonOpen;
    QFrame *frame;
    QGridLayout *gridLayout_4;
    QPushButton *pushButtonConnect;
    QComboBox *comboBox;
    QPushButton *pushButton_3;
    QComboBox *comboBox_7;
    QDockWidget *dockWidgetOutline;
    QWidget *dockWidgetRightContents;
    QGridLayout *gridLayout_14;
    QTabWidget *tabWidgetOutline;
    QWidget *tabOutline;
    QGridLayout *gridLayout_15;
    QListWidget *listWidgetOutline;
    QWidget *tab_4;
    QGridLayout *gridLayout_23;
    QLabel *label;
    QComboBox *comboBox_5;
    QLabel *label_14;
    QComboBox *comboBox_6;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QWidget *tab3d;
    QGridLayout *gridLayout_24;
    QCheckBox *checkBox_3;
    QDockWidget *dockWidgetConsole;
    QWidget *dockWidgetSceneContents;
    QGridLayout *gridLayout_12;
    QTextBrowser *textBrowser;
    QDockWidget *dockWidgetDetails;
    QWidget *dockWidgetContents_4;
    QGridLayout *gridLayout_16;
    QTabWidget *tabWidget;
    QWidget *tabDetails;
    QGridLayout *gridLayout_17;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_18;
    QFrame *line;
    QFrame *frame_4;
    QGridLayout *gridLayout_21;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_6;
    QLineEdit *lineEdit;
    QFrame *frame_5;
    QGridLayout *gridLayout_22;
    QRadioButton *radioButton_5;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_3;
    QComboBox *comboBox_3;
    QComboBox *comboBox_2;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_19;
    QLabel *label_9;
    QLineEdit *lineEditNodeName;
    QLabel *label_10;
    QComboBox *comboBox_4;
    QLabel *label_11;
    QLineEdit *lineEditNodeType;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_20;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox;
    QLabel *label_12;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_13;
    QDoubleSpinBox *doubleSpinBox_2;
    QWidget *tabInputs;
    QWidget *tabOutputs;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1753, 962);
        QIcon icon;
        icon.addFile(QStringLiteral(":/images/fms-icon-desktop.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setStyleSheet(QStringLiteral(""));
        actionE_xit = new QAction(MainWindow);
        actionE_xit->setObjectName(QStringLiteral("actionE_xit"));
        action_New = new QAction(MainWindow);
        action_New->setObjectName(QStringLiteral("action_New"));
        action_Open = new QAction(MainWindow);
        action_Open->setObjectName(QStringLiteral("action_Open"));
        action_Save = new QAction(MainWindow);
        action_Save->setObjectName(QStringLiteral("action_Save"));
        action_Add_Node = new QAction(MainWindow);
        action_Add_Node->setObjectName(QStringLiteral("action_Add_Node"));
        actionSave_As = new QAction(MainWindow);
        actionSave_As->setObjectName(QStringLiteral("actionSave_As"));
        actionOpen_Recent = new QAction(MainWindow);
        actionOpen_Recent->setObjectName(QStringLiteral("actionOpen_Recent"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tabWidget_2 = new QTabWidget(centralWidget);
        tabWidget_2->setObjectName(QStringLiteral("tabWidget_2"));
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        tabWidget_2->addTab(tab_5, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QStringLiteral("tab_6"));
        tabWidget_2->addTab(tab_6, QString());

        gridLayout->addWidget(tabWidget_2, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1753, 19));
        menuE_xit = new QMenu(menuBar);
        menuE_xit->setObjectName(QStringLiteral("menuE_xit"));
        menu_Edit = new QMenu(menuBar);
        menu_Edit->setObjectName(QStringLiteral("menu_Edit"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        dockWidgetNodes = new QDockWidget(MainWindow);
        dockWidgetNodes->setObjectName(QStringLiteral("dockWidgetNodes"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(dockWidgetNodes->sizePolicy().hasHeightForWidth());
        dockWidgetNodes->setSizePolicy(sizePolicy);
        dockWidgetNodes->setMinimumSize(QSize(273, 150));
        dockWidgetLeftContents = new QWidget();
        dockWidgetLeftContents->setObjectName(QStringLiteral("dockWidgetLeftContents"));
        gridLayout_3 = new QGridLayout(dockWidgetLeftContents);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        scrollArea = new QScrollArea(dockWidgetLeftContents);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 271, 718));
        gridLayout_8 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        gridLayout_8->setContentsMargins(0, 0, 0, 0);
        tabWidgetNodes = new QTabWidget(scrollAreaWidgetContents);
        tabWidgetNodes->setObjectName(QStringLiteral("tabWidgetNodes"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        gridLayout_9 = new QGridLayout(tab);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        gridLayout_9->setContentsMargins(0, 0, 0, 0);
        treeViewModels = new QTreeView(tab);
        treeViewModels->setObjectName(QStringLiteral("treeViewModels"));
        treeViewModels->setFrameShape(QFrame::StyledPanel);
        treeViewModels->setDragEnabled(true);
        treeViewModels->setDragDropOverwriteMode(true);
        treeViewModels->setDragDropMode(QAbstractItemView::DragOnly);
        treeViewModels->setDefaultDropAction(Qt::CopyAction);
        treeViewModels->setAlternatingRowColors(true);
        treeViewModels->setSelectionMode(QAbstractItemView::SingleSelection);
        treeViewModels->setIconSize(QSize(5, 5));
        treeViewModels->setRootIsDecorated(true);
        treeViewModels->header()->setVisible(false);

        gridLayout_9->addWidget(treeViewModels, 0, 0, 1, 1);

        tabWidgetNodes->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout_10 = new QGridLayout(tab_2);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        gridLayout_10->setContentsMargins(0, 0, 0, 0);
        treeViewManagers = new QTreeView(tab_2);
        treeViewManagers->setObjectName(QStringLiteral("treeViewManagers"));
        treeViewManagers->setHeaderHidden(true);

        gridLayout_10->addWidget(treeViewManagers, 0, 0, 1, 1);

        tabWidgetNodes->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        gridLayout_11 = new QGridLayout(tab_3);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        gridLayout_11->setContentsMargins(0, 0, 0, 0);
        treeViewWidgets = new QTreeView(tab_3);
        treeViewWidgets->setObjectName(QStringLiteral("treeViewWidgets"));
        treeViewWidgets->setHeaderHidden(true);

        gridLayout_11->addWidget(treeViewWidgets, 0, 0, 1, 1);

        tabWidgetNodes->addTab(tab_3, QString());

        gridLayout_8->addWidget(tabWidgetNodes, 3, 0, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_3->addWidget(scrollArea, 0, 0, 1, 1);

        dockWidgetNodes->setWidget(dockWidgetLeftContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(1), dockWidgetNodes);
        dockWidgetToolbar = new QDockWidget(MainWindow);
        dockWidgetToolbar->setObjectName(QStringLiteral("dockWidgetToolbar"));
        dockWidgetToolbar->setMinimumSize(QSize(1527, 100));
        dockWidgetToolbar->setMaximumSize(QSize(524287, 100));
        dockWidgetTopContents = new QWidget();
        dockWidgetTopContents->setObjectName(QStringLiteral("dockWidgetTopContents"));
        gridLayout_2 = new QGridLayout(dockWidgetTopContents);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        frame_3 = new QFrame(dockWidgetTopContents);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        gridLayout_6 = new QGridLayout(frame_3);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        gridLayout_6->setContentsMargins(2, 2, 2, 2);
        label_4 = new QLabel(frame_3);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_6->addWidget(label_4, 0, 1, 1, 1);

        label_3 = new QLabel(frame_3);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_6->addWidget(label_3, 0, 0, 1, 1);

        label_5 = new QLabel(frame_3);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_6->addWidget(label_5, 1, 0, 1, 1);

        label_6 = new QLabel(frame_3);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_6->addWidget(label_6, 2, 0, 1, 1);

        label_7 = new QLabel(frame_3);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_6->addWidget(label_7, 1, 1, 1, 1);

        label_8 = new QLabel(frame_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_6->addWidget(label_8, 2, 1, 1, 1);


        gridLayout_2->addWidget(frame_3, 0, 14, 1, 1);

        pushButtonSchedule = new QPushButton(dockWidgetTopContents);
        pushButtonSchedule->setObjectName(QStringLiteral("pushButtonSchedule"));
        pushButtonSchedule->setMinimumSize(QSize(82, 62));
        pushButtonSchedule->setMaximumSize(QSize(62, 62));

        gridLayout_2->addWidget(pushButtonSchedule, 0, 11, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 0, 15, 1, 1);

        label_2 = new QLabel(dockWidgetTopContents);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/images/NASALogo.png")));

        gridLayout_2->addWidget(label_2, 0, 16, 1, 1);

        pushButtonStop = new QPushButton(dockWidgetTopContents);
        pushButtonStop->setObjectName(QStringLiteral("pushButtonStop"));
        pushButtonStop->setMinimumSize(QSize(82, 62));
        pushButtonStop->setMaximumSize(QSize(62, 62));

        gridLayout_2->addWidget(pushButtonStop, 0, 8, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 0, 13, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 0, 17, 1, 1);

        pushButtonSave = new QPushButton(dockWidgetTopContents);
        pushButtonSave->setObjectName(QStringLiteral("pushButtonSave"));
        pushButtonSave->setMinimumSize(QSize(82, 62));
        pushButtonSave->setMaximumSize(QSize(62, 62));

        gridLayout_2->addWidget(pushButtonSave, 0, 4, 1, 1);

        horizontalSpacer = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 0, 6, 1, 1);

        pushButtonStep = new QPushButton(dockWidgetTopContents);
        pushButtonStep->setObjectName(QStringLiteral("pushButtonStep"));
        pushButtonStep->setMinimumSize(QSize(82, 62));
        pushButtonStep->setMaximumSize(QSize(62, 62));

        gridLayout_2->addWidget(pushButtonStep, 0, 10, 1, 1);

        pushButtonStart = new QPushButton(dockWidgetTopContents);
        pushButtonStart->setObjectName(QStringLiteral("pushButtonStart"));
        pushButtonStart->setMinimumSize(QSize(82, 62));
        pushButtonStart->setMaximumSize(QSize(62, 62));

        gridLayout_2->addWidget(pushButtonStart, 0, 7, 1, 1);

        pushButtonNew = new QPushButton(dockWidgetTopContents);
        pushButtonNew->setObjectName(QStringLiteral("pushButtonNew"));
        pushButtonNew->setMinimumSize(QSize(82, 62));
        pushButtonNew->setMaximumSize(QSize(62, 62));

        gridLayout_2->addWidget(pushButtonNew, 0, 1, 1, 1);

        pushButtonPause = new QPushButton(dockWidgetTopContents);
        pushButtonPause->setObjectName(QStringLiteral("pushButtonPause"));
        pushButtonPause->setMinimumSize(QSize(82, 62));
        pushButtonPause->setMaximumSize(QSize(62, 62));

        gridLayout_2->addWidget(pushButtonPause, 0, 9, 1, 1);

        lcdNumber = new QLCDNumber(dockWidgetTopContents);
        lcdNumber->setObjectName(QStringLiteral("lcdNumber"));
        lcdNumber->setMinimumSize(QSize(250, 62));
        lcdNumber->setMaximumSize(QSize(600, 62));
        lcdNumber->setDigitCount(11);

        gridLayout_2->addWidget(lcdNumber, 0, 12, 1, 1);

        pushButtonOpen = new QPushButton(dockWidgetTopContents);
        pushButtonOpen->setObjectName(QStringLiteral("pushButtonOpen"));
        pushButtonOpen->setMinimumSize(QSize(82, 62));
        pushButtonOpen->setMaximumSize(QSize(62, 62));

        gridLayout_2->addWidget(pushButtonOpen, 0, 2, 1, 1);

        frame = new QFrame(dockWidgetTopContents);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setMinimumSize(QSize(260, 0));
        frame->setMaximumSize(QSize(260, 16777215));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_4 = new QGridLayout(frame);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        pushButtonConnect = new QPushButton(frame);
        pushButtonConnect->setObjectName(QStringLiteral("pushButtonConnect"));
        pushButtonConnect->setMaximumSize(QSize(20, 16777215));

        gridLayout_4->addWidget(pushButtonConnect, 0, 0, 1, 1);

        comboBox = new QComboBox(frame);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setMinimumSize(QSize(56, 0));
        comboBox->setEditable(true);

        gridLayout_4->addWidget(comboBox, 0, 1, 1, 1);

        pushButton_3 = new QPushButton(frame);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        gridLayout_4->addWidget(pushButton_3, 1, 0, 1, 1);

        comboBox_7 = new QComboBox(frame);
        comboBox_7->setObjectName(QStringLiteral("comboBox_7"));
        comboBox_7->setEditable(true);

        gridLayout_4->addWidget(comboBox_7, 1, 1, 1, 1);


        gridLayout_2->addWidget(frame, 0, 0, 1, 1);

        dockWidgetToolbar->setWidget(dockWidgetTopContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(4), dockWidgetToolbar);
        dockWidgetOutline = new QDockWidget(MainWindow);
        dockWidgetOutline->setObjectName(QStringLiteral("dockWidgetOutline"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(dockWidgetOutline->sizePolicy().hasHeightForWidth());
        dockWidgetOutline->setSizePolicy(sizePolicy1);
        dockWidgetOutline->setMinimumSize(QSize(273, 162));
        dockWidgetOutline->setMaximumSize(QSize(524287, 524287));
        dockWidgetRightContents = new QWidget();
        dockWidgetRightContents->setObjectName(QStringLiteral("dockWidgetRightContents"));
        gridLayout_14 = new QGridLayout(dockWidgetRightContents);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        gridLayout_14->setContentsMargins(0, 0, 0, 0);
        tabWidgetOutline = new QTabWidget(dockWidgetRightContents);
        tabWidgetOutline->setObjectName(QStringLiteral("tabWidgetOutline"));
        tabWidgetOutline->setMaximumSize(QSize(16777215, 16777215));
        tabOutline = new QWidget();
        tabOutline->setObjectName(QStringLiteral("tabOutline"));
        gridLayout_15 = new QGridLayout(tabOutline);
        gridLayout_15->setSpacing(6);
        gridLayout_15->setContentsMargins(11, 11, 11, 11);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        gridLayout_15->setContentsMargins(0, 0, 0, 0);
        listWidgetOutline = new QListWidget(tabOutline);
        listWidgetOutline->setObjectName(QStringLiteral("listWidgetOutline"));
        sizePolicy1.setHeightForWidth(listWidgetOutline->sizePolicy().hasHeightForWidth());
        listWidgetOutline->setSizePolicy(sizePolicy1);
        listWidgetOutline->setMinimumSize(QSize(0, 0));
        listWidgetOutline->setMaximumSize(QSize(16777215, 16777215));

        gridLayout_15->addWidget(listWidgetOutline, 0, 0, 1, 1);

        tabWidgetOutline->addTab(tabOutline, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        gridLayout_23 = new QGridLayout(tab_4);
        gridLayout_23->setSpacing(6);
        gridLayout_23->setContentsMargins(11, 11, 11, 11);
        gridLayout_23->setObjectName(QStringLiteral("gridLayout_23"));
        label = new QLabel(tab_4);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_23->addWidget(label, 0, 0, 1, 1);

        comboBox_5 = new QComboBox(tab_4);
        comboBox_5->setObjectName(QStringLiteral("comboBox_5"));

        gridLayout_23->addWidget(comboBox_5, 0, 1, 1, 1);

        label_14 = new QLabel(tab_4);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_23->addWidget(label_14, 1, 0, 1, 1);

        comboBox_6 = new QComboBox(tab_4);
        comboBox_6->setObjectName(QStringLiteral("comboBox_6"));

        gridLayout_23->addWidget(comboBox_6, 1, 1, 1, 1);

        pushButton = new QPushButton(tab_4);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout_23->addWidget(pushButton, 2, 0, 1, 1);

        pushButton_2 = new QPushButton(tab_4);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        gridLayout_23->addWidget(pushButton_2, 2, 1, 1, 1);

        tabWidgetOutline->addTab(tab_4, QString());
        tab3d = new QWidget();
        tab3d->setObjectName(QStringLiteral("tab3d"));
        gridLayout_24 = new QGridLayout(tab3d);
        gridLayout_24->setSpacing(6);
        gridLayout_24->setContentsMargins(11, 11, 11, 11);
        gridLayout_24->setObjectName(QStringLiteral("gridLayout_24"));
        checkBox_3 = new QCheckBox(tab3d);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));

        gridLayout_24->addWidget(checkBox_3, 0, 0, 1, 1);

        tabWidgetOutline->addTab(tab3d, QString());

        gridLayout_14->addWidget(tabWidgetOutline, 0, 0, 1, 1);

        dockWidgetOutline->setWidget(dockWidgetRightContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), dockWidgetOutline);
        dockWidgetConsole = new QDockWidget(MainWindow);
        dockWidgetConsole->setObjectName(QStringLiteral("dockWidgetConsole"));
        sizePolicy1.setHeightForWidth(dockWidgetConsole->sizePolicy().hasHeightForWidth());
        dockWidgetConsole->setSizePolicy(sizePolicy1);
        dockWidgetConsole->setMinimumSize(QSize(88, 71));
        dockWidgetConsole->setMaximumSize(QSize(524287, 524287));
        dockWidgetSceneContents = new QWidget();
        dockWidgetSceneContents->setObjectName(QStringLiteral("dockWidgetSceneContents"));
        gridLayout_12 = new QGridLayout(dockWidgetSceneContents);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        gridLayout_12->setContentsMargins(0, 0, 0, 0);
        textBrowser = new QTextBrowser(dockWidgetSceneContents);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        gridLayout_12->addWidget(textBrowser, 0, 0, 1, 1);

        dockWidgetConsole->setWidget(dockWidgetSceneContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(8), dockWidgetConsole);
        dockWidgetDetails = new QDockWidget(MainWindow);
        dockWidgetDetails->setObjectName(QStringLiteral("dockWidgetDetails"));
        dockWidgetDetails->setMinimumSize(QSize(268, 576));
        dockWidgetDetails->setMaximumSize(QSize(524287, 524287));
        dockWidgetContents_4 = new QWidget();
        dockWidgetContents_4->setObjectName(QStringLiteral("dockWidgetContents_4"));
        gridLayout_16 = new QGridLayout(dockWidgetContents_4);
        gridLayout_16->setSpacing(6);
        gridLayout_16->setContentsMargins(11, 11, 11, 11);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        gridLayout_16->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(dockWidgetContents_4);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabDetails = new QWidget();
        tabDetails->setObjectName(QStringLiteral("tabDetails"));
        gridLayout_17 = new QGridLayout(tabDetails);
        gridLayout_17->setSpacing(6);
        gridLayout_17->setContentsMargins(11, 11, 11, 11);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        groupBox = new QGroupBox(tabDetails);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMaximumSize(QSize(16777215, 210));
        gridLayout_18 = new QGridLayout(groupBox);
        gridLayout_18->setSpacing(6);
        gridLayout_18->setContentsMargins(11, 11, 11, 11);
        gridLayout_18->setObjectName(QStringLiteral("gridLayout_18"));
        line = new QFrame(groupBox);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_18->addWidget(line, 6, 0, 1, 1);

        frame_4 = new QFrame(groupBox);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        gridLayout_21 = new QGridLayout(frame_4);
        gridLayout_21->setSpacing(6);
        gridLayout_21->setContentsMargins(11, 11, 11, 11);
        gridLayout_21->setObjectName(QStringLiteral("gridLayout_21"));
        gridLayout_21->setContentsMargins(0, 0, 0, 0);
        radioButton = new QRadioButton(frame_4);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setChecked(true);

        gridLayout_21->addWidget(radioButton, 0, 0, 1, 1);

        radioButton_2 = new QRadioButton(frame_4);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));

        gridLayout_21->addWidget(radioButton_2, 1, 0, 1, 1);

        radioButton_6 = new QRadioButton(frame_4);
        radioButton_6->setObjectName(QStringLiteral("radioButton_6"));

        gridLayout_21->addWidget(radioButton_6, 2, 0, 1, 1);

        lineEdit = new QLineEdit(frame_4);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        gridLayout_21->addWidget(lineEdit, 2, 1, 1, 1);


        gridLayout_18->addWidget(frame_4, 3, 0, 1, 1);

        frame_5 = new QFrame(groupBox);
        frame_5->setObjectName(QStringLiteral("frame_5"));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        gridLayout_22 = new QGridLayout(frame_5);
        gridLayout_22->setSpacing(6);
        gridLayout_22->setContentsMargins(11, 11, 11, 11);
        gridLayout_22->setObjectName(QStringLiteral("gridLayout_22"));
        gridLayout_22->setContentsMargins(0, 0, 0, 0);
        radioButton_5 = new QRadioButton(frame_5);
        radioButton_5->setObjectName(QStringLiteral("radioButton_5"));

        gridLayout_22->addWidget(radioButton_5, 1, 0, 1, 1);

        radioButton_4 = new QRadioButton(frame_5);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));

        gridLayout_22->addWidget(radioButton_4, 0, 0, 1, 1);

        radioButton_3 = new QRadioButton(frame_5);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));

        gridLayout_22->addWidget(radioButton_3, 2, 0, 1, 1);

        comboBox_3 = new QComboBox(frame_5);
        comboBox_3->setObjectName(QStringLiteral("comboBox_3"));

        gridLayout_22->addWidget(comboBox_3, 1, 1, 1, 1);

        comboBox_2 = new QComboBox(frame_5);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));

        gridLayout_22->addWidget(comboBox_2, 2, 1, 1, 1);


        gridLayout_18->addWidget(frame_5, 7, 0, 1, 1);


        gridLayout_17->addWidget(groupBox, 1, 0, 1, 1);

        groupBox_2 = new QGroupBox(tabDetails);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 150));
        gridLayout_19 = new QGridLayout(groupBox_2);
        gridLayout_19->setSpacing(6);
        gridLayout_19->setContentsMargins(11, 11, 11, 11);
        gridLayout_19->setObjectName(QStringLiteral("gridLayout_19"));
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_19->addWidget(label_9, 0, 0, 1, 1);

        lineEditNodeName = new QLineEdit(groupBox_2);
        lineEditNodeName->setObjectName(QStringLiteral("lineEditNodeName"));
        lineEditNodeName->setMinimumSize(QSize(70, 0));
        lineEditNodeName->setMaximumSize(QSize(150, 16777215));

        gridLayout_19->addWidget(lineEditNodeName, 1, 0, 1, 1);

        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_19->addWidget(label_10, 0, 1, 1, 1);

        comboBox_4 = new QComboBox(groupBox_2);
        comboBox_4->setObjectName(QStringLiteral("comboBox_4"));
        comboBox_4->setMaximumSize(QSize(10, 16777215));
        comboBox_4->setEditable(true);

        gridLayout_19->addWidget(comboBox_4, 1, 1, 1, 1);

        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_19->addWidget(label_11, 2, 0, 1, 1);

        lineEditNodeType = new QLineEdit(groupBox_2);
        lineEditNodeType->setObjectName(QStringLiteral("lineEditNodeType"));

        gridLayout_19->addWidget(lineEditNodeType, 3, 0, 1, 1);


        gridLayout_17->addWidget(groupBox_2, 0, 0, 1, 1);

        groupBox_3 = new QGroupBox(tabDetails);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setMaximumSize(QSize(16777215, 150));
        gridLayout_20 = new QGridLayout(groupBox_3);
        gridLayout_20->setSpacing(6);
        gridLayout_20->setContentsMargins(11, 11, 11, 11);
        gridLayout_20->setObjectName(QStringLiteral("gridLayout_20"));
        checkBox_2 = new QCheckBox(groupBox_3);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));

        gridLayout_20->addWidget(checkBox_2, 1, 0, 1, 1);

        checkBox = new QCheckBox(groupBox_3);
        checkBox->setObjectName(QStringLiteral("checkBox"));

        gridLayout_20->addWidget(checkBox, 0, 0, 1, 1);

        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_20->addWidget(label_12, 3, 0, 1, 1);

        doubleSpinBox = new QDoubleSpinBox(groupBox_3);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setMaximumSize(QSize(75, 16777215));

        gridLayout_20->addWidget(doubleSpinBox, 3, 1, 1, 1);

        label_13 = new QLabel(groupBox_3);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_20->addWidget(label_13, 4, 0, 1, 1);

        doubleSpinBox_2 = new QDoubleSpinBox(groupBox_3);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setMaximumSize(QSize(75, 16777215));

        gridLayout_20->addWidget(doubleSpinBox_2, 4, 1, 1, 1);


        gridLayout_17->addWidget(groupBox_3, 2, 0, 1, 1);

        tabWidget->addTab(tabDetails, QString());
        tabInputs = new QWidget();
        tabInputs->setObjectName(QStringLiteral("tabInputs"));
        tabWidget->addTab(tabInputs, QString());
        tabOutputs = new QWidget();
        tabOutputs->setObjectName(QStringLiteral("tabOutputs"));
        tabWidget->addTab(tabOutputs, QString());

        gridLayout_16->addWidget(tabWidget, 0, 0, 1, 1);

        dockWidgetDetails->setWidget(dockWidgetContents_4);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), dockWidgetDetails);

        menuBar->addAction(menuE_xit->menuAction());
        menuBar->addAction(menu_Edit->menuAction());
        menuE_xit->addAction(action_New);
        menuE_xit->addAction(action_Open);
        menuE_xit->addAction(actionOpen_Recent);
        menuE_xit->addAction(action_Save);
        menuE_xit->addAction(actionSave_As);
        menuE_xit->addSeparator();
        menuE_xit->addAction(actionE_xit);
        menuE_xit->addSeparator();
        menu_Edit->addAction(action_Add_Node);

        retranslateUi(MainWindow);

        tabWidgetNodes->setCurrentIndex(0);
        tabWidgetOutline->setCurrentIndex(0);
        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Fault Management Simulator", 0));
        actionE_xit->setText(QApplication::translate("MainWindow", "E&xit", 0));
        action_New->setText(QApplication::translate("MainWindow", "&New", 0));
        action_Open->setText(QApplication::translate("MainWindow", "&Open", 0));
        action_Save->setText(QApplication::translate("MainWindow", "&Save", 0));
        action_Add_Node->setText(QApplication::translate("MainWindow", "&Add Node", 0));
        actionSave_As->setText(QApplication::translate("MainWindow", "Save As...", 0));
        actionOpen_Recent->setText(QApplication::translate("MainWindow", "Open Recent...", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_5), QApplication::translate("MainWindow", "Tab 1", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_6), QApplication::translate("MainWindow", "Tab 2", 0));
        menuE_xit->setTitle(QApplication::translate("MainWindow", "&File", 0));
        menu_Edit->setTitle(QApplication::translate("MainWindow", "&Edit", 0));
        tabWidgetNodes->setTabText(tabWidgetNodes->indexOf(tab), QApplication::translate("MainWindow", "Models", 0));
        tabWidgetNodes->setTabText(tabWidgetNodes->indexOf(tab_2), QApplication::translate("MainWindow", "Managers", 0));
        tabWidgetNodes->setTabText(tabWidgetNodes->indexOf(tab_3), QApplication::translate("MainWindow", "Widgets", 0));
        label_4->setText(QApplication::translate("MainWindow", "0:00:00:000", 0));
        label_3->setText(QApplication::translate("MainWindow", "Start Time: ", 0));
        label_5->setText(QApplication::translate("MainWindow", "End Time:", 0));
        label_6->setText(QApplication::translate("MainWindow", "Duration:", 0));
        label_7->setText(QApplication::translate("MainWindow", "0:00:00:000", 0));
        label_8->setText(QApplication::translate("MainWindow", "0:00:00:000", 0));
        pushButtonSchedule->setText(QApplication::translate("MainWindow", "Schedule", 0));
        label_2->setText(QString());
        pushButtonStop->setText(QApplication::translate("MainWindow", "Stop", 0));
        pushButtonSave->setText(QApplication::translate("MainWindow", "Save", 0));
        pushButtonStep->setText(QApplication::translate("MainWindow", "Step", 0));
        pushButtonStart->setText(QApplication::translate("MainWindow", "Start", 0));
        pushButtonNew->setText(QApplication::translate("MainWindow", "New", 0));
        pushButtonPause->setText(QApplication::translate("MainWindow", "Pause", 0));
        pushButtonOpen->setText(QApplication::translate("MainWindow", "Open", 0));
        pushButtonConnect->setText(QApplication::translate("MainWindow", "Connect", 0));
        pushButton_3->setText(QApplication::translate("MainWindow", "Project", 0));
        tabWidgetOutline->setTabText(tabWidgetOutline->indexOf(tabOutline), QApplication::translate("MainWindow", "Outline", 0));
        label->setText(QApplication::translate("MainWindow", "Release", 0));
        label_14->setText(QApplication::translate("MainWindow", "Manager", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Load", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "Save", 0));
        tabWidgetOutline->setTabText(tabWidgetOutline->indexOf(tab_4), QApplication::translate("MainWindow", "Test Cases", 0));
        checkBox_3->setText(QApplication::translate("MainWindow", "Enable 3D Viewport", 0));
        tabWidgetOutline->setTabText(tabWidgetOutline->indexOf(tab3d), QApplication::translate("MainWindow", "3D", 0));
        groupBox->setTitle(QApplication::translate("MainWindow", "Version", 0));
        radioButton->setText(QApplication::translate("MainWindow", "Local", 0));
        radioButton_2->setText(QApplication::translate("MainWindow", "Head", 0));
        radioButton_6->setText(QApplication::translate("MainWindow", "Custom", 0));
        radioButton_5->setText(QApplication::translate("MainWindow", "Branches", 0));
        radioButton_4->setText(QApplication::translate("MainWindow", "Trunk", 0));
        radioButton_3->setText(QApplication::translate("MainWindow", "Tags", 0));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "General", 0));
        label_9->setText(QApplication::translate("MainWindow", "Name", 0));
        lineEditNodeName->setText(QApplication::translate("MainWindow", "PDCU", 0));
        label_10->setText(QApplication::translate("MainWindow", "ID", 0));
        comboBox_4->setCurrentText(QString());
        label_11->setText(QApplication::translate("MainWindow", "Type", 0));
        lineEditNodeType->setText(QApplication::translate("MainWindow", "Model", 0));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Runtime", 0));
        checkBox_2->setText(QApplication::translate("MainWindow", "Record Output", 0));
        checkBox->setText(QApplication::translate("MainWindow", "Record Input", 0));
        label_12->setText(QApplication::translate("MainWindow", "Start Time", 0));
        label_13->setText(QApplication::translate("MainWindow", "End Time", 0));
        tabWidget->setTabText(tabWidget->indexOf(tabDetails), QApplication::translate("MainWindow", "Details", 0));
        tabWidget->setTabText(tabWidget->indexOf(tabInputs), QApplication::translate("MainWindow", "Inputs", 0));
        tabWidget->setTabText(tabWidget->indexOf(tabOutputs), QApplication::translate("MainWindow", "Outputs", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
