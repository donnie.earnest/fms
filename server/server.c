#include <zmq.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

int main (void)
{
    void *context = zmq_ctx_new ();
    void *responder = zmq_socket (context, ZMQ_REP);
    int rc = zmq_bind (responder, "tcp://*:5555");
    assert (rc == 0);

    while (1) {
        char buffer [6];
        char msg[6];

        zmq_recv (responder, buffer, 5, 0);
        strncpy(msg, buffer, 5);
        msg[5] = '\0';

        if (!strcmp(msg,"01001" )) {
            zmq_send (responder, "01001", 5, 0);
        }
        else if (!strcmp(msg,"01000" )) {
        	zmq_send (responder, "01000", 5, 0);
        	}
        else {
        	zmq_send (responder, "99999", 5, 0);
        	sleep(5);
        	exit(0);
        }
    }
    return 0;
}

