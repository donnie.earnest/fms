/*!
  @file    QNodeViewBlock.cpp

  Copyright (c) 2014 Graham Wihlidal

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  @author  Graham Wihlidal
  @date    January 19, 2014
*/

#include <QPen>
#include <QGraphicsScene>
#include <QFontMetrics>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include <QPushButton>
#include <QGraphicsProxyWidget>
#include <QNodeViewBlock.h>
#include <QNodeViewPort.h>

#include <QDebug>

QNodeViewBlock::QNodeViewBlock(QGraphicsItem* parent)
: QGraphicsPathItem(parent)
, m_width(120)
, m_height(10)
, m_horizontalMargin(10)
, m_verticalMargin(5)
{
    setCacheMode(DeviceCoordinateCache);

    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemIsSelectable);
}


QNodeViewBlock::~QNodeViewBlock()
{
}

QNodeViewPort* QNodeViewBlock::addPort(const QString& name, bool isOutput, qint32 flags, qint32 index)
{
    QNodeViewPort* port = new QNodeViewPort(this);
	port->setName(name);
	port->setIsOutput(isOutput);
    port->setBlock(this);
	port->setPortFlags(flags);
    port->setIndex(index);

    QFontMetrics fontMetrics(scene()->font());
    const qint32 width  = fontMetrics.width(name);
    const qint32 height = fontMetrics.height()+5;

    if (width > m_width - m_horizontalMargin)
        m_width = width + m_horizontalMargin;

    m_height += height;

    QPainterPath path;
    path.addRoundedRect(-(m_width >> 1), -(m_height >> 1), m_width, m_height, 5, 5);
    setPath(path);

    qint32 y = -(m_height >> 1) + m_verticalMargin + port->radius();

    Q_FOREACH (QGraphicsItem* childItem, childItems())
    {
        if (childItem->type() != QNodeViewType_Port)
			continue;

        QNodeViewPort* childPort = static_cast<QNodeViewPort*>(childItem);

        if (childPort->isOutput())
            childPort->setPos((m_width >> 1) + childPort->radius(), y);
		else
            childPort->setPos(-(m_width >> 1) - childPort->radius(), y);

        y += height;
	}

    return port;
}

QString QNodeViewBlock::getName()
{
    Q_FOREACH (QGraphicsItem* childItem, childItems())
    {
        if (childItem->type() == QNodeViewType_Block)
            continue;

        if (childItem->type() == QNodeViewType_Port){
            QNodeViewPort* port = static_cast<QNodeViewPort*>(childItem);
            qint32 flags = port->portFlags();
            if (flags == 1)
                m_name = port->portName();
        }
    }
    return m_name;
}


void QNodeViewBlock::addInputPort(const QString& name)
{
	addPort(name, false);
}

void QNodeViewBlock::addOutputPort(const QString& name)
{
	addPort(name, true);
}

void QNodeViewBlock::addWidget(const QString& name){

    std::string s = name.toStdString();
    if (s.find("Button") == 0){
        //QPainterPath path_widget;
        //path_widget.addRect(-50,-50,100,100);
        //setPath(path_widget);
        //painter->setPen(Qt::NoPen);
        //painter->drawPath(path_widget.simplified());

        QGraphicsProxyWidget* myproxy = new QGraphicsProxyWidget(this);
        QPushButton* nodeWidget = new QPushButton("Button");
        myproxy->setWidget(nodeWidget);
    }
}

void QNodeViewBlock::addInputPorts(const QStringList& names)
{
    Q_FOREACH (const QString& name, names)
        addInputPort(name);
}

void QNodeViewBlock::addOutputPorts(const QStringList& names)
{
    Q_FOREACH (const QString& name, names)
        addOutputPort(name);
}

void QNodeViewBlock::save(QDataStream& stream)
{
    stream << pos();

    qint32 count = 0;

    Q_FOREACH (QGraphicsItem* childItem, childItems())
	{
        if (childItem->type() != QNodeViewType_Port)
			continue;

        ++count;
	}

    stream << count;

    Q_FOREACH (QGraphicsItem* childItem, childItems())
	{
        if (childItem->type() != QNodeViewType_Port)
			continue;

        QNodeViewPort* port = static_cast<QNodeViewPort*>(childItem);
        stream << reinterpret_cast<quint64>(port);
        stream << port->portName();
        stream << port->isOutput();
        stream << port->portFlags();
	}
}

void QNodeViewBlock::load(QDataStream& stream, QMap<quint64, QNodeViewPort*>& portMap)
{
    QPointF position;
    stream >> position;
    setPos(position);

    qint32 count;
    stream >> count;

    for (qint32 iter = 0; iter < count; iter++)
	{
        quint64 index;
        stream >> index;

        QString name;
        stream >> name;

        bool output;
        stream >> output;

        qint32 flags;
        stream >> flags;

        portMap[index] = addPort(name, output, flags, index);
	}
}

void QNodeViewBlock::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    Q_UNUSED(widget);

    // Only paint dirty regions for increased performance
    painter->setClipRect(option->exposedRect);

    const qreal title_height = 30;
    QPainterPath path_title;
    path_title.setFillRule(Qt::WindingFill);
    path_title.addRoundedRect(QRect(-(m_width/2), -(m_height/2), m_width, title_height - 5), 5, 5);
    path_title.addRect(-(m_width/2), -(m_height/2) + 6, m_width, title_height);
    painter->setPen(Qt::NoPen);
    painter->setBrush(QColor(59, 114, 171));
    painter->drawPath(path_title.simplified());

    if (isSelected())
    {
        QPen pen(QColor(241, 176, 0));
        pen.setWidth(3);
        painter->setPen(pen);
        painter->setBrush(QColor(0,0,0,128));
    }
    else
    {
        QPen pen(QColor(0, 0, 0));
        pen.setWidth(2);
        painter->setPen(pen);
        painter->setBrush(QColor(0,0,0,128));
    }

    painter->drawPath(path());
}

QNodeViewBlock* QNodeViewBlock::clone()
{
    QNodeViewBlock* block = new QNodeViewBlock(NULL);
    this->scene()->addItem(block);

    Q_FOREACH (QGraphicsItem* childPort, childItems())
	{
        if (childPort->type() == QNodeViewType_Port)
		{
            QNodeViewPort* clonePort = static_cast<QNodeViewPort*>(childPort);
            block->addPort(
                    clonePort->portName(),
                    clonePort->isOutput(),
                    clonePort->portFlags(),
                    clonePort->index());
		}
	}

    return block;
}

QVector<QNodeViewPort*> QNodeViewBlock::ports()
{
    QVector<QNodeViewPort*> result;

    Q_FOREACH (QGraphicsItem* childItem, childItems())
	{
        if (childItem->type() == QNodeViewType_Port)
            result.append(static_cast<QNodeViewPort*>(childItem));
	}

    return result;
}

QVariant QNodeViewBlock::itemChange(GraphicsItemChange change, const QVariant& value)
{
    Q_UNUSED(change);
	return value;
}
