# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'W:\FMS\trunk\client\fms_gui_prefs.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(644, 413)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/Images/NASALogo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.tabWidgetPrefs = QtWidgets.QTabWidget(Dialog)
        self.tabWidgetPrefs.setObjectName("tabWidgetPrefs")
        self.tabPrefsAppearance = QtWidgets.QWidget()
        self.tabPrefsAppearance.setObjectName("tabPrefsAppearance")
        self.comboBoxThemes = QtWidgets.QComboBox(self.tabPrefsAppearance)
        self.comboBoxThemes.setGeometry(QtCore.QRect(30, 50, 191, 22))
        self.comboBoxThemes.setObjectName("comboBoxThemes")
        self.label = QtWidgets.QLabel(self.tabPrefsAppearance)
        self.label.setGeometry(QtCore.QRect(30, 30, 91, 16))
        self.label.setObjectName("label")
        self.tabWidgetPrefs.addTab(self.tabPrefsAppearance, "")
        self.tabPrefsNetwork = QtWidgets.QWidget()
        self.tabPrefsNetwork.setObjectName("tabPrefsNetwork")
        self.tabWidgetPrefs.addTab(self.tabPrefsNetwork, "")
        self.gridLayout.addWidget(self.tabWidgetPrefs, 0, 0, 1, 1)
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName("gridLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 0, 0, 1, 1)
        self.pushButtonOk = QtWidgets.QPushButton(self.frame)
        self.pushButtonOk.setObjectName("pushButtonOk")
        self.gridLayout_2.addWidget(self.pushButtonOk, 0, 1, 1, 1)
        self.pushButtonApply = QtWidgets.QPushButton(self.frame)
        self.pushButtonApply.setObjectName("pushButtonApply")
        self.gridLayout_2.addWidget(self.pushButtonApply, 0, 2, 1, 1)
        self.pushButtonCancel = QtWidgets.QPushButton(self.frame)
        self.pushButtonCancel.setObjectName("pushButtonCancel")
        self.gridLayout_2.addWidget(self.pushButtonCancel, 0, 3, 1, 1)
        self.gridLayout.addWidget(self.frame, 1, 0, 1, 1)

        self.retranslateUi(Dialog)
        self.tabWidgetPrefs.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "FMS Preferences"))
        self.label.setText(_translate("Dialog", "Color Theme:"))
        self.tabWidgetPrefs.setTabText(self.tabWidgetPrefs.indexOf(self.tabPrefsAppearance), _translate("Dialog", "Appearance"))
        self.tabWidgetPrefs.setTabText(self.tabWidgetPrefs.indexOf(self.tabPrefsNetwork), _translate("Dialog", "Network"))
        self.pushButtonOk.setText(_translate("Dialog", "OK"))
        self.pushButtonApply.setText(_translate("Dialog", "Apply"))
        self.pushButtonCancel.setText(_translate("Dialog", "Cancel"))

import fms_gui_rc_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

