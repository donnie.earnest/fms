#include <QGuiApplication>
#include <QGraphicsScene>
#include <QFileDialog>
#include <QTime>
#include <QFile>
#include <QElapsedTimer>

#include <QNodeViewBlock.h>
#include <QNodeViewEditor.h>
#include <QNodeViewPort.h>
#include <QNodeViewCanvas.h>

#include <FileDownloader.h>
#include <XMLWorker.h>
#include <QFileInfo>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QString>
#include <QStringList>

#include "MainWindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QDir>

QString fmsconfig = QDir::homePath() + "/.mfmtools/fmsconfig.xml";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setTheme("Moonrise");

    //pretending we get this value from our local fmsconfig.xml file
    //QString nodes = "http://mfmdev.msfc.nasa.gov:8000/trac/chrome/site/MyXML.xml" ;
    //startDownload(nodes);
    // need a loop or something here to wait for download to complete
    Filename = "SLS_Block1.xml";

    //outline = new QStringListModel(this);
    //ui->listViewOutline->setModel(outline);

    defineButtons();
    defineNodeview();
    defineCounter();
    defineTreeViews();
    defineProgressBar();

    logMessage("Ready");
}

void MainWindow::defineButtons()
{
    QObject::connect(ui->pushButtonStart, SIGNAL(clicked()), this, SLOT(startSim()));
    QObject::connect(ui->pushButtonStop, SIGNAL(clicked()), this, SLOT(stopSim()));
    QObject::connect(ui->pushButtonPause, SIGNAL(clicked()), this, SLOT(pauseSim()));
    QObject::connect(ui->pushButtonNew, SIGNAL(clicked()), this, SLOT(newFile()));
    QObject::connect(ui->pushButtonSave, SIGNAL(clicked()), this, SLOT(saveFile()));
    QObject::connect(ui->pushButtonOpen, SIGNAL(clicked()), this, SLOT(loadFile()));
}

void MainWindow::defineCounter()
{
    minutes=0;
    seconds=0;
    timer = new QTimer();
    timeValue = new QTime(0,minutes,seconds,0);
    ui->lcdNumber->display(timeValue->toString(QString("h:mm:ss:zzz")));
    connect(timer, SIGNAL(timeout()), this, SLOT(setDisplay()));
}

void MainWindow::defineNodeview()
{
    m_scene = new QGraphicsScene();
    m_view = new QNodeViewCanvas(m_scene, this);
    m_view->setAcceptDrops(true);
    setCentralWidget(m_view);

    m_editor = new QNodeViewEditor(this);
    m_editor->install(m_scene);

    connect(m_view, SIGNAL(nodeDropped()), this, SLOT(addBlock()));
    connect(m_editor, SIGNAL(nodeSelected(QString)), this, SLOT(updateDetails(QString)));
}

void MainWindow::defineTreeViews()
{
    // Models
    ui->treeViewModels->setDragEnabled(true);
    models = new QStandardItemModel(0,1,this);
    ui->treeViewModels->setModel(models);
    readNodes("Models","System","Model");
    ui->treeViewModels->expandAll();

    // Managers
    ui->treeViewManagers->setDragEnabled(true);
    managers = new QStandardItemModel(0,1,this);
    ui->treeViewManagers->setModel(managers);
    readNodes("Managers","Manager","Subsystem");
    ui->treeViewManagers->expandAll();

    // Widgets
    ui->treeViewWidgets->setDragEnabled(true);
    widgets = new QStandardItemModel(0,1,this);
    ui->treeViewWidgets->setModel(widgets);
    readNodes("Widgets","Type","Widget");
    ui->treeViewWidgets->expandAll();
}

void MainWindow::defineProgressBar()
{
    QProgressBar *progressBar = new QProgressBar(this);
    progressBar->setValue(25);
    progressBar->setMaximumWidth(273);
    progressBar->setMaximumHeight(10);
    progressBar->setTextVisible(false);
    ui->statusBar->addPermanentWidget(progressBar);
}

void MainWindow::startDownload(const QUrl &url)
{
    QString path = url.path();
    QString filename = QFileInfo(path).fileName();
    logMessage("Downloading: " + filename);
    output.setFileName(filename);
    if (!output.open(QIODevice::WriteOnly)) {
        logMessage("Error downloading:" + filename);
        return;
    }

    QNetworkRequest request(url);
    currentDownload = manager.get(request);
    connect(currentDownload, SIGNAL(downloadProgress(qint64,qint64)),
            SLOT(downloadProgress(qint64,qint64)));
    connect(currentDownload, SIGNAL(finished()),
            SLOT(downloadFinished()));
    connect(currentDownload, SIGNAL(readyRead()),
            SLOT(downloadReadyRead()));

    downloadTime.start();
}

void MainWindow::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    progressBar.setMaximum(bytesTotal);
    progressBar.setValue(bytesReceived);
    progressBar.show();
}

void MainWindow::downloadFinished()
{
    progressBar.reset();
    output.close();
    currentDownload->deleteLater();
    logMessage("Download complete");
}

void MainWindow::downloadReadyRead()
{
    output.write(currentDownload->readAll());
}

void MainWindow::readNodes(const QString& node, const QString& category, const QString& item)
{
    QStandardItem *root = new QStandardItem(node);

    if (node == "Models")
        models->appendRow(root);
    else if (node == "Managers")
        managers->appendRow(root);
    else if (node == "Widgets")
        widgets->appendRow(root);

    QDomDocument document;

    QFile file(Filename);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        document.setContent(&file);
        file.close();
    }

    QDomElement xmlroot = document.firstChildElement();

    QDomNodeList cats = xmlroot.elementsByTagName(category);
    for(int i = 0; i < cats.count(); i++)
    {
        QDomElement cat = cats.at(i).toElement();
        QStandardItem *catitem = new QStandardItem(cat.attribute("Name"));

        QDomNodeList subs = cat.elementsByTagName(item);
        for(int h = 0; h < subs.count(); h++)
        {
            QDomElement sub = subs.at(h).toElement();
            QStandardItem *subitem = new QStandardItem(sub.attribute("Name"));

            catitem->appendRow(subitem);
        }
        root->appendRow(catitem);
    }
}

void MainWindow::setTheme(const QString& theme)
{
    QString filename = "themes/" + theme + ".css";
    QFile file(filename);
    file.open(QFile::ReadOnly);
    QString style = QLatin1String(file.readAll());
    this->setStyleSheet(style);
}

void MainWindow::addBlock()
{    
    addBlockInternal(this->mapFromGlobal(QCursor::pos()));
}

void MainWindow::updateDetails(QString name)
{
    logMessage(name + " selected");

    /*
    QVariant data;
    QString blockName, blockType;

    qint8 tab = ui->tabWidgetNodes->currentIndex();
    if (tab == 0)
    {
        blockType = "Model";
        QModelIndex index = ui->treeViewModels->currentIndex();
        data = ui->treeViewModels->model()->data(index);
    }
    //ui->lineEditNodeName->setText(blockName);
    //ui->lineEditNodeType->setText(blockType);
    */

}

void MainWindow::newFile()
{
    m_scene->clear();
}

void MainWindow::saveFile()
{
    QString s_fileName = QFileDialog::getSaveFileName();
    if (s_fileName.isEmpty())
        return;

    QFile f_file(s_fileName);
    f_file.open(QFile::WriteOnly);
    QDataStream stream(&f_file);
    m_editor->save(stream);
}

void MainWindow::loadFile()
{
    QString s_fileName = QFileDialog::getOpenFileName();
    if (s_fileName.isEmpty())
        return;

    QFile f_file(s_fileName);
    f_file.open(QFile::ReadOnly);
    QDataStream stream(&f_file);
    m_editor->load(stream);
}

void MainWindow::startSim()
{
    logMessage("Simulation started");
    timer->setInterval(1);
    timer->start();
}

void MainWindow::stopSim()
{
    logMessage("Simulation stopped");
    timer->stop();
    timeValue = new QTime(0,0,0,0);
    ui->lcdNumber->display(timeValue->toString(QString("h:mm:ss:zzz")));

}

void MainWindow::pauseSim()
{
    logMessage("Simulation paused");
    timer->stop();
}

void MainWindow::addBlockInternal(const QPointF& position)
{
    QVariant data;
    QString blockName, blockType;

    QNodeViewBlock* block = new QNodeViewBlock(NULL);
    m_scene->addItem(block);

    // need to count number of existing models with name and add 1
    //static qint32 index = 1;
    //blockName = QString("testModel_%1").arg(index++);
    qint8 tab = ui->tabWidgetNodes->currentIndex();
    if (tab == 0)
    {
        blockType = "Model";
        QModelIndex index = ui->treeViewModels->currentIndex();
        data = ui->treeViewModels->model()->data(index);
        blockName = data.toString() + " 1";
        block->addPort(blockName, 0, QNodeViewPortLabel_Name);
        block->addPort(blockType, 0, QNodeViewPortLabel_Type);
        block->addInputPort("Input 1");
        block->addInputPort("Input 2");
        block->addInputPort("Input 3");
        block->addOutputPort("Output 1");
        block->addOutputPort("Output 2");
        block->addOutputPort("Output 3");
    }
    else if (tab == 1)
    {
        blockType = "Manager";
        QModelIndex index = ui->treeViewManagers->currentIndex();
        data = ui->treeViewManagers->model()->data(index);
        blockName = data.toString() + " 1";
        block->addPort(blockName, 0, QNodeViewPortLabel_Name);
        block->addPort(blockType, 0, QNodeViewPortLabel_Type);
        block->addInputPort("Input 1");
        block->addInputPort("Input 2");
        block->addInputPort("Input 3");
        block->addOutputPort("Output 1");
        block->addOutputPort("Output 2");
        block->addOutputPort("Output 3");
    }
    else if (tab == 2)
    {
        blockType = "Widget";
        QModelIndex index = ui->treeViewWidgets->currentIndex();
        data = ui->treeViewWidgets->model()->data(index);

        /*
        //blockName = data.toString();
        QGraphicsProxyWidget* myproxy = new QGraphicsProxyWidget(block);

        if (blockName == "Button")
        {
            QPushButton* nodeWidget = new QPushButton("Button");
            myproxy->setWidget(nodeWidget);
        };
        */

        blockName = data.toString() + "_1";
        block->addPort(blockName, 0, QNodeViewPortLabel_Name);
        block->addPort(blockType, 0, QNodeViewPortLabel_Type);

        QString parent = index.parent().data().toString();
        if (parent == "Inputs"){
            block->addOutputPort("Output");
        }else{
            block->addInputPort("Input");
        }

        //block->addWidget(blockName);

    }

    /*
    int row = ui->listViewOutline->currentIndex().row();
    outline->insertRows(row,1);
    QModelIndex index = outline->index(outline->rowCount()-1);
    outline->setData(index, blockName);
    */

    ui->listWidgetOutline->addItem(blockName);
    ui->listWidgetOutline->sortItems();

    logMessage(blockType + " " + blockName + " added to simulation");
    block->setPos(position);
}

void MainWindow::setDisplay()
{
    this->timeValue->setHMS(0,this->timeValue->addMSecs(-1).minute(),
                        this->timeValue->addMSecs(-1).second(),
                        this->timeValue->addMSecs(-1).msec());
    ui->lcdNumber->display(timeValue->toString(QString("h:mm:ss:zzz")));
}

void MainWindow::showTime()
{
    QTime time = QTime::currentTime();
    QString text = time.toString("hh:mm");
    if ((time.second() % 2 == 0))
        text[2] = ' ';
    ui->lcdNumber->display(text);
}

void MainWindow::logMessage(const QString& msg)
{
    if (debug)
        qDebug() << msg;

    if (msg == "Ready"){
        ui->statusBar->showMessage(tr("Ready"),10000);
    }
    else{
        ui->statusBar->showMessage(msg,2000);
        ui->textBrowser->append(msg);
        ui->textBrowser->ensureCursorVisible();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
