import zmq

print("Connecting to hello world server...")
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:5555")
socket.send(b"Hello")
message = socket.recv()
print("Received reply [ %s ]" % message)
