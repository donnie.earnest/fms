#-------------------------------------------------
#
# Project created by QtCreator 2016-06-06T09:26:43
#
#-------------------------------------------------

QT += core xml gui network
DEFINES  += QT_NO_SSL
CONFIG += static

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fmsclient
TEMPLATE = app

SOURCES += main.cpp\
        QNodeViewBlock.cpp \
        QNodeViewCanvas.cpp \
        QNodeViewConnection.cpp \
        QNodeViewEditor.cpp \
        QNodeViewPort.cpp \
        MainWindow.cpp

HEADERS  += \
        QNodeViewBlock.h \
        QNodeViewCanvas.h \
        QNodeViewCommon.h \
        QNodeViewConnection.h \
        QNodeViewEditor.h \
        QNodeViewPort.h \
        MainWindow.h \
        resource.h

FORMS    += mainwindow.ui

cache()

RESOURCES += \
        fmsclient.qrc
