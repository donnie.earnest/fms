#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QtXml>
#include <QDebug>
#include <QFile>
#include <QQueue>
#include <QTime>
#include <QUrl>
#include <QNetworkAccessManager>

class QNodeViewEditor;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QTimer* timer;
    QTime*  timeValue;
    int minutes;
    int seconds;
    bool debug = false;
    bool downloadComplete = false;
    QString saveFileName(const QUrl &url);

signals:
    void finished();

private slots:
    void newFile();
    void saveFile();
    void loadFile();
    void startSim();
    void stopSim();
    void pauseSim();
    void startDownload(const QUrl &url);
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void downloadFinished();
    void downloadReadyRead();

public slots:
    void addBlock();
    void updateDetails(QString name);
    void setDisplay();
    void showTime();

private:
    Ui::MainWindow *ui;
    QStandardItemModel *models;
    QStandardItemModel *managers;
    QStandardItemModel *widgets;
    //QStringListModel *outline;
    QString Filename;
    QNetworkAccessManager manager;
    QQueue<QUrl> downloadQueue;
    QNetworkReply *currentDownload;
    QFile output;
    QTime downloadTime;
    QProgressBar progressBar;
    int downloadedCount;
    int totalCount;
    void timeOut();
    void defineButtons();
    void defineCounter();
    void defineNodeview();
    void defineTreeViews();
    void defineProgressBar();
    void readNodes(const QString& node, const QString& category, const QString& item);
    void logMessage(const QString& msg);
    void setTheme(const QString& theme);
    void addBlockInternal(const QPointF& position);

private:
    QNodeViewEditor* m_editor;
    QMenu* m_fileMenu;
    QGraphicsView* m_view;
    QGraphicsScene* m_scene;

};

#endif // MAINWINDOW_H
