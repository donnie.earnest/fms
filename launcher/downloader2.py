from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtNetwork
import sys


class Download(QtNetwork.QNetworkAccessManager):

    def slot_finished(self, dfile):
        file = QtCore.QFile("images/" + dfile)

        if file.open(QtCore.QIODevice.WriteOnly):
            file.write(self.messageBuffer)
            file.close()
            QtWidgets.QMessageBox.information(None, "Hello!", "File has been saved!")
        else:
            QtWidgets.QMessageBox.critical(None, "Hello!", "Error saving file!")

    # Append current data to the buffer every time readyRead() signal is emitted
    def slot_read_data(self):
        self.messageBuffer += self.reply.readAll()

    def __init__(self, link, file):
        QtNetwork.QNetworkAccessManager.__init__(self)
        self.messageBuffer = QtCore.QByteArray()
        url = QtCore.QUrl(link + file)
        req = QtNetwork.QNetworkRequest(url)
        self.reply = self.get(req)
        self.reply.readyRead.connect(self.slot_read_data)
        self.reply.finished.connect(lambda: self.slot_finished(file))


# End of NetworkAccessManager Class
def main():
    app = QtWidgets.QApplication(sys.argv)
    dl = Download('http://mfmlab.msfc.nasa.gov/', 'easetupfull.exe')
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
