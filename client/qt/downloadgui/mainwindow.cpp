#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QObject>
#include <QFileInfo>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QString>
#include <QStringList>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QObject::connect(ui->pushButtonExit, SIGNAL(clicked()), this, SLOT(close()));
    QObject::connect(ui->pushButtonDownload, SIGNAL(clicked()), this, SLOT(startDownload()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startDownload()
{
    QUrl url = ui->lineEditURL->text();

    QString path = url.path();
    QString filename = QFileInfo(path).fileName();

    ui->labelStatus->setText("Downloading: " + filename);

    output.setFileName(filename);
    if (!output.open(QIODevice::WriteOnly)) {
        ui->labelStatus->setText("Error downloading:" + filename);
        return;
    }

    QNetworkRequest request(url);
    currentDownload = manager.get(request);
    connect(currentDownload, SIGNAL(downloadProgress(qint64,qint64)),
            SLOT(downloadProgress(qint64,qint64)));
    connect(currentDownload, SIGNAL(finished()),
            SLOT(downloadFinished()));
    connect(currentDownload, SIGNAL(readyRead()),
            SLOT(downloadReadyRead()));

    downloadTime.start();
}

void MainWindow::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    ui->progressBar->setMaximum(bytesTotal);
    ui->progressBar->setValue(bytesReceived);
    ui->progressBar->show();
}

void MainWindow::downloadFinished()
{
    ui->progressBar->reset();
    output.close();
    currentDownload->deleteLater();
}

void MainWindow::downloadReadyRead()
{
    output.write(currentDownload->readAll());
}


