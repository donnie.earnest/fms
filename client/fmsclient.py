import os
import sys
import select
import paramiko
import zmq
import socketserver as SocketServer
import threading
from threading import Event
from time import sleep
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import Qt, pyqtSlot, pyqtSignal, QTimer
from PyQt5.QtGui import QColor, QBrush
import fms_gui
import fms_gui_prefs
import fms_gui_about
import fms_model

# Global variables
fm_server = "localhost"
fm_port = 5555
sh_port = 2222
g_verbose = True
TICK_TIME = 2**6
zmq_server = ""
socket = ""


class MainWindow(QtWidgets.QMainWindow, fms_gui.Ui_MainWindow):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.actionExit.triggered.connect(fms_exit)
        self.actionAbout_FMS.triggered.connect(self.menu_about)
        self.actionPreferences_2.triggered.connect(self.menu_prefs)
        self.pushButtonStart.clicked.connect(self.do_start)
        self.pushButtonPause.clicked.connect(self.do_pause)
        self.pushButtonStop.clicked.connect(self.do_reset)
        self.pushButtonConnect.clicked.connect(connect_to_server)
        self.pushButtonNew.clicked.connect(self.add_model)
        self.pushButtonOpen.clicked.connect(disconnect_server)

        self.mdiArea = QtWidgets.QMdiArea()
        self.mdiArea.setViewMode(1)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/Images/simulation_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.mdiArea.setWindowIcon(icon)
        self.mdiArea.setTabsClosable(True)
        self.mdiArea.setTabsMovable(True)

        c1 = QColor(26, 33, 35)
        c2 = QColor(17, 21, 22)
        b1 = QBrush(c2)
        self.mdiArea.setBackground(b1)
        self.setCentralWidget(self.mdiArea)

        self.progressBar.setValue(50)

        self.timer = QTimer()
        self.timer.setInterval(TICK_TIME)
        self.timer.timeout.connect(self.tick)
        self.do_reset()

    def closeEvent(self, event):
        if socket:
            zmq_server.shutdown()

    def display(self):
        self.lcdNumber.setDigitCount(11)
        # self.lcdNumber.display("%d:%05.2f" % (self.time // 60, self.time % 60))
        minutes = 0
        seconds = 0
        timeValue = QtCore.QTime(0, minutes, seconds, 0)
        self.lcdNumber.display(timeValue.toString('hh:mm:ss:zzz'))

    def set_style(self, css):
        css = "themes/" + css + ".css"
        with open(css, "r") as fh:
            self.setStyleSheet(fh.read())

    def menu_about(self):
        about = self.AboutWindow(self)
        about.setModal(True)
        about.exec_()

    def menu_prefs(self):
        prefs = self.PrefsWindow(self)
        prefs.setModal(True)
        prefs.exec_()

    def add_model(self):
        self.mdiArea.addSubWindow(self.subwindow)
        self.subwindow.ensurePolished()
        # for i in range(10):
        #   item = QtWidgets.QListWidgetItem("Item %i" % i)
        #    self.listWidget.addItem(item)
        self.tableWidget.setRowCount(10)
        self.tableWidget.setColumnCount(5)
        self.tableWidget.setHorizontalHeaderLabels(('Model', 'Revision', 'Location', 'StartTime', 'EndTime'))
        # self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.horizontalHeader().setSectionResizeMode(1)
        self.tableWidget.setShowGrid(True)
        self.tableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableWidget.setItem(0, 0, QtWidgets.QTableWidgetItem("PDCU"))
        self.subwindow.show()

    @pyqtSlot()
    def tick(self):
        self.time += TICK_TIME/1000
        self.display()

    @pyqtSlot()
    def do_start(self):
        self.timer.start()

    @pyqtSlot()
    def do_pause(self):
        self.timer.stop()

    @pyqtSlot()
    def do_reset(self):
        self.time = 0
        self.timer.stop()
        self.display()

    class ModelWindow(QtWidgets.QMdiSubWindow, fms_model.Ui_Dialog):
        def __init__(self, parent):
            QtWidgets.QMdiSubWindow.__init__(self, parent)
            self.setupUi(self)

    class AboutWindow(QtWidgets.QDialog, fms_gui_about.Ui_Dialog):
        def __init__(self, parent):
            QtWidgets.QDialog.__init__(self, parent)
            self.setupUi(self)

    class PrefsWindow(QtWidgets.QDialog, fms_gui_prefs.Ui_Dialog):
        def __init__(self, parent):
            QtWidgets.QDialog.__init__(self, parent)
            self.setupUi(self)
            self.pushButtonOk.clicked.connect(self.prefs_ok)
            self.pushButtonApply.clicked.connect(self.prefs_apply)
            self.pushButtonCancel.clicked.connect(self.prefs_cancel)

            for file in os.listdir("themes"):
                if file.endswith(".css"):
                    name = os.path.splitext(file)[0]
                    self.comboBoxThemes.addItem(name)

        def prefs_ok(self):
            current_index = self.tabWidgetPrefs.currentIndex()
            if current_index == 0:
                theme = str(self.comboBoxThemes.currentText())
                fmsapp.set_style(theme)
            self.close()

        def prefs_apply(self):
            current_index = self.tabWidgetPrefs.currentIndex()
            if current_index == 0:
                theme = str(self.comboBoxThemes.currentText())
                fmsapp.set_style(theme)

        def prefs_cancel(self):
            current_index = self.tabWidgetPrefs.currentIndex()
            print(str(current_index))
            self.close()


class WorkerForward (threading.Thread):
    def __init__(self, thread_id, name, ready=None):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.ready = ready

    def run(self):
        verbose("Starting " + self.name)
        server_tunnel(self)
        verbose("Exiting " + self.name)


class WorkerConnect (threading.Thread):
    def __init__(self, thread_id, name, ready=None):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.ready = ready

    def run(self):
        global socket
        if socket:
            socket.close()
        verbose("Starting " + self.name)
        sleep(5)
        log("Connecting to Fault Management Simulation Server...")
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.connect("tcp://localhost:5555")
        socket.send(b"01001")
        message = socket.recv()
        log("Received reply [ %s ]" % message)
        socket.send(b"01000")
        message = socket.recv()
        log("Reveived reply [ %s ]" % message)
        verbose("Exiting " + self.name)
        fmsapp.pushButtonConnect.setText("Disconnect")
        status("Connected to server")


class WorkerDisconnect (threading.Thread):
    def __init__(self, thread_id, name, ready=None):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.ready = ready

    def run(self):
        global socket, zmq_server
        verbose("Starting " + self.name)
        socket.send(b"99999")
        message = socket.recv()
        log("Reveived reply [ %s ]" % message)
        verbose("Exiting " + self.name)
        if socket:
            socket.close()
            zmq_server.shutdown()
        fmsapp.pushButtonConnect.setText("Connect")
        status("Disconnected")


class ForwardServer (SocketServer.ThreadingTCPServer):
    daemon_threads = True
    allow_reuse_address = True


class Handler (SocketServer.BaseRequestHandler):

    def handle(self):
        try:
            chan = self.ssh_transport.open_channel('direct-tcpip',
                                                   (self.chain_host, self.chain_port),
                                                   self.request.getpeername())
        except Exception as e:
            verbose('Incoming request to %s:%d failed: %s' % (self.chain_host,
                                                              self.chain_port,
                                                              repr(e)))
            return
        if chan is None:
            verbose('Incoming request to %s:%d was rejected by the SSH server.' %
                    (self.chain_host, self.chain_port))
            return

        verbose('Connected!  Tunnel open %r -> %r -> %r' % (self.request.getpeername(),
                                                            chan.getpeername(), (self.chain_host, self.chain_port)))
        while True:
            r, w, x = select.select([self.request, chan], [], [])
            if self.request in r:
                data = self.request.recv(1024)
                if len(data) == 0:
                    break
                chan.send(data)
            if chan in r:
                data = chan.recv(1024)
                if len(data) == 0:
                    break
                self.request.send(data)

        peername = self.request.getpeername()
        chan.close()
        self.request.close()
        verbose('Tunnel closed from %r' % (peername,))


def server_tunnel(self):

    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    verbose('Connecting to ssh host %s:%d ...' % (fm_server, sh_port))
    try:
        # client.connect(fm_server, sh_port, username=getpass.getuser(), look_for_keys=True)
        client.connect(fm_server, sh_port, username="dlearnes", look_for_keys=True)
    except Exception as e:
        print('*** Failed to connect to %s:%d: %r' % (fm_server, sh_port, e))
        sys.exit(1)

    try:
        client.exec_command("~/FMS/trunk/server/fmsd")
    except Exception as e:
        print('*** Unable to start FMS Server: %s ...' % e)
        sys.exit(2)

    self.ready.set()

    verbose('Now forwarding port %d to %s:%d ...' % (fm_port, fm_server, fm_port))

    try:
        forward_tunnel(fm_port, fm_server, fm_port, client.get_transport())
    except KeyboardInterrupt:
        print('C-c: Port forwarding stopped.')
        sys.exit(0)


def forward_tunnel(local_port, remote_host, remote_port, transport):

    global zmq_server

    class SubHander (Handler):
        chain_host = remote_host
        chain_port = remote_port
        ssh_transport = transport

    zmq_server = ForwardServer(('', local_port), SubHander)
    zmq_server.serve_forever()


def connect_to_server():
    if socket:
        disconnect_server()
    else:
        ready = Event()
        thread_tunnel = WorkerForward(1, "WorkerForward", ready)
        thread_tunnel.start()
        thread_connect = WorkerConnect(1, "WorkerConnect", ready)
        thread_connect.start()


def disconnect_server():
    log("Disconnecting from FMS Server...")
    ready = Event()
    thread_disconnect = WorkerDisconnect(1, "WorkerDisconnect", ready)
    thread_disconnect.start()


def verbose(s):
    if g_verbose:
        s = "VERBOSE: " + s
        fmsapp.textEditOutput.append(s)


def status(txt):
    fmsapp.label_status.setText(txt)


def log(txt):
    fmsapp.textEditOutput.append(txt)
    fmsapp.textEditOutput.ensureCursorVisible()


def fms_exit(rc):
    sys.exit(rc)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    fmsapp = MainWindow()
    # fmsapp.set_style("Solarized")
    fmsapp.show()
    sys.exit(app.exec_())
