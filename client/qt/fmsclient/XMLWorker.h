#ifndef QXMLWORKER_H
#define QXMLWORKER_H

#include <MainWindow.h>

class QXMLWorker
{
public:
    QXMLWorker();
    void readConfig();
};

#endif // QXMLWORKER_H
